package com.ab4demo.app.ws.entity;

import com.ab4demo.app.ws.domain.SportActivities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "locations")
public class LocationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @Column
    private String cityName;

    @Column
    private String regionName;

    @Column
    private String countryName;

    @OneToMany(mappedBy = "locationEntity", cascade = CascadeType.ALL)
    private List<SportActivitiesEntity> sportActivitiesEntities;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public List<SportActivitiesEntity> getSportActivitiesEntities() {
        return sportActivitiesEntities;
    }

    public void setSportActivitiesEntities(List<SportActivitiesEntity> sportActivitiesEntities) {
        this.sportActivitiesEntities = sportActivitiesEntities;
    }
}

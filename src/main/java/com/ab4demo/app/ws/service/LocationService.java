package com.ab4demo.app.ws.service;

import com.ab4demo.app.ws.api.LocationRequest;
import com.ab4demo.app.ws.api.LocationResponse;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

public interface LocationService {
    LocationResponse createLocation(LocationRequest locationRequest);
    LocationResponse getLocationByName(String name);
    LocationResponse updateLocation(String name, LocationRequest locationRequest);
    void deleteLocation(String name);

    List<LocationResponse> getAllLocationsBySports(Collection<String> sport);
    List<LocationResponse> getAllLocationsByDate(Collection<LocalDate> seasonStartDate, Collection<LocalDate> seasonEndDate);
}

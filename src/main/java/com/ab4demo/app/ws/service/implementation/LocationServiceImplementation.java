package com.ab4demo.app.ws.service.implementation;

import com.ab4demo.app.ws.api.LocationRequest;
import com.ab4demo.app.ws.api.LocationResponse;
import com.ab4demo.app.ws.entity.LocationEntity;
import com.ab4demo.app.ws.entity.SportActivitiesEntity;
import com.ab4demo.app.ws.repository.LocationRepository;
import com.ab4demo.app.ws.repository.SportActivitiesRepository;
import com.ab4demo.app.ws.service.LocationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LocationServiceImplementation implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private SportActivitiesRepository sportActivitiesRepository;

    @Override
    public LocationResponse createLocation(LocationRequest locationRequest) {

        if (locationRepository.findByName(locationRequest.getName()) != null)
            throw new RuntimeException("Sorry, but this location already exists...");


        LocationEntity locationEntity = new LocationEntity();

        locationEntity.setName(locationRequest.getName());
        locationEntity.setCityName(locationRequest.getCityName());
        locationEntity.setRegionName(locationRequest.getRegionName());
        locationEntity.setCountryName(locationRequest.getCountryName());
        locationRepository.save(locationEntity);

        List<SportActivitiesEntity> sportActivitiesEntities = locationRequest.getSportActivities().stream().map(sportActivity -> {
            SportActivitiesEntity sportActivitiesEntity = new SportActivitiesEntity();
            sportActivitiesEntity.setSport(sportActivity.getSport());
            sportActivitiesEntity.setSeasonEndDate(sportActivity.getSeasonEndDate());
            sportActivitiesEntity.setSeasonStartDate(sportActivity.getSeasonStartDate());
            sportActivitiesEntity.setLocationEntity(locationEntity);
            return sportActivitiesEntity;
        }).collect(Collectors.toList());
        sportActivitiesRepository.saveAll(sportActivitiesEntities);

        LocationResponse locationResponse = new LocationResponse();
        locationResponse.setName(locationRequest.getName());
        locationResponse.setCityName(locationRequest.getCityName());
        locationResponse.setRegionName(locationRequest.getRegionName());
        locationResponse.setCountryName(locationRequest.getCountryName());
        locationResponse.setSportActivities(locationRequest.getSportActivities());

        return locationResponse;
    }

    @Override
    public LocationResponse getLocationByName(String name) {
        LocationEntity locationEntity = locationRepository.findByName(name);

        if(locationEntity == null)
            throw new RuntimeException("Sorry, but this location does not exist..");

        ModelMapper modelMapper = new ModelMapper();
        LocationResponse returnValue = modelMapper.map(locationEntity, LocationResponse.class);

        return returnValue;
    }

    @Override
    public LocationResponse updateLocation(String name, LocationRequest locationRequest) {
        LocationEntity locationEntity = locationRepository.findByName(name);

        if(locationEntity == null)
            throw new RuntimeException("Sorry, but this location you would like to update does not exist...");

        locationEntity.setName(locationRequest.getName());
        locationEntity.setCityName(locationRequest.getCityName());
        locationEntity.setRegionName(locationRequest.getRegionName());
        locationEntity.setCountryName(locationRequest.getCountryName());
        locationRepository.save(locationEntity);

        List<SportActivitiesEntity> sportActivitiesEntities = locationRequest.getSportActivities().stream().map(sportActivity -> {
            SportActivitiesEntity sportActivitiesEntity = new SportActivitiesEntity();
            sportActivitiesEntity.setSport(sportActivity.getSport());
            sportActivitiesEntity.setSeasonEndDate(sportActivity.getSeasonEndDate());
            sportActivitiesEntity.setSeasonStartDate(sportActivity.getSeasonStartDate());
            sportActivitiesEntity.setLocationEntity(locationEntity);
            return sportActivitiesEntity;
        }).collect(Collectors.toList());
        sportActivitiesRepository.saveAll(sportActivitiesEntities);

        LocationEntity updatedLocationDetails = locationRepository.save(locationEntity);
        LocationResponse returnValue = new ModelMapper().map(updatedLocationDetails, LocationResponse.class);

        return returnValue;
    }       // updates a location and, if necessary, you can add more sport activities.

    @Override
    public void deleteLocation(String name) {
        LocationEntity locationEntity = locationRepository.findByName(name);

        if (locationEntity == null)
            throw new RuntimeException("Sorry but this location does not even exist...");

        locationRepository.delete(locationEntity);
    }

    @Override
    public List<LocationResponse> getAllLocationsBySports(Collection<String> sport) {
        List<LocationEntity> locationEntities = locationRepository.findBySports(sport);
        List<LocationResponse> locationResponses = new ArrayList<>();

        if (locationEntities.size() == 0 && sport.size() == 1)
            throw new RuntimeException("Sorry but there is no location you can find it by this sport");
        else if (locationEntities.size() == 0 && sport.size() > 1)
            throw new RuntimeException("Sorry but there is no location you can find it by these sports");
        else {
            for (LocationEntity locationEntity : locationEntities) {
                LocationResponse locationResponse = new LocationResponse();
                locationResponses.add(locationResponse);
                locationResponse.setName(locationEntity.getName());
                locationResponse.setCityName(locationEntity.getCityName());
                locationResponse.setRegionName(locationEntity.getRegionName());
                locationResponse.setCountryName(locationEntity.getCountryName());
            }
            return locationResponses;
        }
    }

    @Override
    public List<LocationResponse> getAllLocationsByDate(Collection<LocalDate> seasonStartDate, Collection<LocalDate> seasonEndDate) {
        List<LocationEntity> locationEntities = locationRepository.findByLocalDates(seasonStartDate,seasonEndDate);
        List<LocationResponse> locationResponses = new ArrayList<>();

        if (locationEntities.size() == 0)
            throw new RuntimeException("Sorry but there is no location you can find in this period...");
        else {
            for (LocationEntity locationEntity : locationEntities) {
                LocationResponse locationResponse = new LocationResponse();
                locationResponses.add(locationResponse);
                locationResponse.setName(locationEntity.getName());
                locationResponse.setCityName(locationEntity.getCityName());
                locationResponse.setRegionName(locationEntity.getRegionName());
                locationResponse.setCountryName(locationEntity.getCountryName());
            }
            return locationResponses;
        }
    }


}

package com.ab4demo.app.ws.repository;

import com.ab4demo.app.ws.entity.LocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<LocationEntity, Long> {
    LocationEntity findByName(String name);
    @Query(value = "FROM LocationEntity Locations INNER JOIN Locations.sportActivitiesEntities sports WHERE sports.sport IN ?1")
    List<LocationEntity> findBySports(Collection<String> sports);
    @Query(value = "FROM LocationEntity Locations INNER JOIN Locations.sportActivitiesEntities sports WHERE sports.seasonStartDate BETWEEN ?1 AND ?2")
    List<LocationEntity> findByLocalDates(Collection<LocalDate> seasonStartDate, Collection<LocalDate> seasonEndDate);
}

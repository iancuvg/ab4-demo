package com.ab4demo.app.ws.repository;

import com.ab4demo.app.ws.entity.SportActivitiesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SportActivitiesRepository extends JpaRepository<SportActivitiesEntity, Long> {
}

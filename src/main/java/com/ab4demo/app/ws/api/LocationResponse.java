package com.ab4demo.app.ws.api;

import com.ab4demo.app.ws.domain.Location;
import com.ab4demo.app.ws.domain.SportActivities;

import java.util.List;

public class LocationResponse {

    private String name;
    private String cityName;
    private String regionName;
    private String countryName;
    private List<SportActivities> sportActivities;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public List<SportActivities> getSportActivities() {
        return sportActivities;
    }

    public void setSportActivities(List<SportActivities> sportActivities) {
        this.sportActivities = sportActivities;
    }
}

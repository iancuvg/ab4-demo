package com.ab4demo.app.ws.controller;

import com.ab4demo.app.ws.api.LocationRequest;
import com.ab4demo.app.ws.api.LocationResponse;
import com.ab4demo.app.ws.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@RestController
public class LocationController {

    @Autowired
    private LocationService locationService;

    @PostMapping("/location")
    public ResponseEntity<LocationResponse> createLocation(@RequestBody LocationRequest locationRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(locationService.createLocation(locationRequest));
    }

    @GetMapping("/location/{name}")
    public ResponseEntity<LocationResponse> getLocationByName(@PathVariable String name) {
        return ResponseEntity.status(HttpStatus.OK).body(locationService.getLocationByName(name));
    }

    @PutMapping("/location/{name}")
    public ResponseEntity<LocationResponse> updateLocation(@PathVariable String name, @RequestBody LocationRequest locationRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(locationService.updateLocation(name,locationRequest));
    }

    @GetMapping("/locationsBySports")
    public ResponseEntity<List<LocationResponse>> getAllLocationsBySports(@RequestParam Collection<String> sport) {
        return ResponseEntity.status(HttpStatus.OK).body(locationService.getAllLocationsBySports(sport));
    }					// Get mapping cu metoda creata in jparepository CU query

    @GetMapping("/locationsBySeason")
    public ResponseEntity<List<LocationResponse>> getAllLocationsBySeason(@RequestParam("seasonStartDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Collection<LocalDate> seasonStartDate,
                                                                          @RequestParam("seasonEndDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Collection<LocalDate> seasonEndDate) {
        return ResponseEntity.status(HttpStatus.OK).body(locationService.getAllLocationsByDate(seasonStartDate,seasonEndDate));
    }

    @DeleteMapping("/location/{name}")
    public void deleteLocation(@PathVariable String name) {
        locationService.deleteLocation(name);
    }
}
